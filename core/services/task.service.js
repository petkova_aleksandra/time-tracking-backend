const Task = require('../../database/models/task.model');

module.exports.create = async (task) => {
  const latestCreatedTask = await Task.findOne({ 'project.shortName': task.project.shortName }).sort({ 'createdAt': -1 });
  const latestIdentifier = latestCreatedTask ? parseInt(latestCreatedTask.identifier) : 0;

  const taskModel = new Task({
    title: task.title,
    identifier: latestIdentifier + 1,
    project: {
      projectId: task.project.projectId,
      shortName: task.project.shortName
    },
    assignee: {
      userId: task.assignee.userId,
      username: task.assignee.username,
      email: task.assignee.email
    },
    priority: task.priority,
    type: task.type,
    description: task.description,
    estimationInHours: task.estimationInHours,
    spentHours: task.spentHours
  });

  // TODO validate if the user is assigned to the project

  return await taskModel.save();
}

module.exports.update = async (id, task) => {
  await Task.findOneAndUpdate({ _id : id}, task);
}

module.exports.delete = async (id) => {
  await Task.findByIdAndRemove(id);
}

module.exports.findAll = async (identifier, title, projectId, assigneeId, status, priority) => {
  let query = {};

  if (identifier) {
    let identifierParts = identifier.split('-');
    query["project.shortName"] = identifierParts[0];
    query["identifier"] = identifierParts[1];
  }
  
  if (title) {
    query["title"] = title;
  }

  if (projectId) {
    query["project.projectId"] = projectId;
  }

  if (assigneeId) {
    query["assignee.userId"] = assigneeId;
  }

  if (status) {
    query["status"] = status;
  }

  if (priority) {
    query["priority"] = priority;
  }

  console.log('Coworkers tasks search query : ');
  console.log('query["project.shortName"] = ' + query["project.shortName"]);
  console.log('query["identifier"] = ' + query["identifier"]);
  console.log('query["title"] = ' + query["title"]);
  console.log('query["project.projectId"] = ' + query["project.projectId"]);
  console.log('query["assignee.userId"] = ' + query["assignee.userId"]);
  console.log('query["status"] = ' + query["status"]);
  console.log('query["priority"] = ' + query["priority"]);

  return await Task.find(query);
}
const Report = require('../../database/models/report.model');
const Project = require('../../database/models/project.model');
const User = require('../../database/models/user.model');

const PDFDocument = require('pdfkit');
const fs = require('fs');

const dateUtil = require('../util/date.util');

const mailConfig = require('../configs/mail.config');

const mailService = require('../services/mail.service');

module.exports.createNewCurrentReport = async (currentDate, currentYear, currentMonth, projectId, userId) => {
  const project = await Project.findById(projectId);
  const user = await User.findById(userId);

  const foundProjectUser = project.users.filter(projectUser => JSON.stringify(projectUser.userId) === JSON.stringify(userId));
  console.log("Found project user details " + foundProjectUser[0]);

  const dailySpentTime = await this.calculateReportDaysInCurrentMonth(currentDate);
  const weeks = await this.calculateReportWeeksInCurrentMonth(currentDate);

  const reportModel = new Report({
    year: currentYear,
    month: currentMonth,
    project: {
      projectId: project._id,
      shortName: project.shortName
    },
    assignee: {
      userId: user._id,
      username: user.username,
      email: user.email
    },
    montlySpentTime: 0,
    weeks: weeks,
    dailySpentTime: dailySpentTime,
    maxMontlySpentTime: foundProjectUser[0].maxMonthHours,
    maxWeeklySpentTime: foundProjectUser[0].maxWeekHours,
    maxDailySpentTime: foundProjectUser[0].maxDayHours
  });

  const report = await reportModel.save();
  const currentWeek = await this.getCurrentWeek(report.weeks);

  return {
    _id: report._id,
    year: report.year,
    month: report.month,
    day: currentDate.getDate(),
    week: {
      _id: currentWeek._id,
      weekNumber: currentWeek.weekNumber,
      startDate: currentWeek.startDate,
      endDate: currentWeek.endDate,
      weeklySpentTime: currentWeek.weeklySpentTime
    },
    montlySpentTime: 0,
    dailySpentTime: 0,
    maxMontlySpentTime: foundProjectUser[0].maxMonthHours,
    maxWeeklySpentTime: foundProjectUser[0].maxWeekHours,
    maxDailySpentTime: foundProjectUser[0].maxDayHours
  }
}

module.exports.calculateReportDaysInCurrentMonth = async (currentDate) => {
  const numberOfDaysInMonth = dateUtil.getNumberOfDaysInMonth(currentDate);

  let dailySpentTime = [];
  for (let index = 0; index < numberOfDaysInMonth; index++) {
    dailySpentTime[index] = 0;
  }
  return dailySpentTime;
}

module.exports.calculateReportWeeksInCurrentMonth = async (currentDate) => {
  const weeks = [];
  let index = 1;
  let indexDate = dateUtil.getFirstDateOfMonth(currentDate);
  let endDate;
  const numberOfDaysInMonth = dateUtil.getLastDayOfMonth(currentDate);
  const lastDateInMonth = dateUtil.getLastDateOfMonth(currentDate);

  while (index <= numberOfDaysInMonth) {
    if (index == 1) {
      switch (indexDate.getDay()) {
        case 0:
          endDate = indexDate;
          index = index + 1;
          break;
        case 1:
          endDate = new Date(indexDate.getTime() + (6 * 24 * 60 * 60 * 1000));
          index = index + 7;
          break;
        case 2:
          endDate = new Date(indexDate.getTime() + (5 * 24 * 60 * 60 * 1000));
          index = index + 6;
          break;
        case 3:
          endDate = new Date(indexDate.getTime() + (4 * 24 * 60 * 60 * 1000));
          index = index + 5;
          break;
        case 4:
          endDate = new Date(indexDate.getTime() + (3 * 24 * 60 * 60 * 1000));
          index = index + 4;
          break;
        case 5:
          endDate = new Date(indexDate.getTime() + (2 * 24 * 60 * 60 * 1000));
          index = index + 3;
          break;
        case 6:
          endDate = new Date(indexDate.getTime() + (1 * 24 * 60 * 60 * 1000));
          index = index + 2;
          break;
      }
      weeks[0] = {
        weekNumber: 1,
        startDate: indexDate,
        endDate: endDate,
        weeklySpentTime: 0
      };
      indexDate = new Date(endDate.getTime() + (1 * 24 * 60 * 60 * 1000));
      continue;
    }

    if (lastDateInMonth.getDate() - indexDate.getDate() < 7) {
      weeks[weeks.length] = {
        weekNumber: weeks.length + 1,
        startDate: indexDate,
        endDate: lastDateInMonth,
        weeklySpentTime: 0
      };
      index = index + 7;
      break;
    }

    endDate = new Date(indexDate.getTime() + (6 * 24 * 60 * 60 * 1000));
    weeks[weeks.length] = {
      weekNumber: weeks.length + 1,
      startDate: indexDate,
      endDate: endDate,
      weeklySpentTime: 0
    };
    indexDate = new Date(endDate.getTime() + (1 * 24 * 60 * 60 * 1000));
    index = index + 7;
  }

  return weeks;
}

module.exports.getCurrentWeek = async (weeks) => {
  const currentDate = dateUtil.getCurrentServerDate();
  const filteredWeeks = weeks
    .filter(week => week.startDate.getTime() <= currentDate.getTime() && week.endDate.getTime() >= currentDate.getTime());
  return filteredWeeks[0];
}

module.exports.findCurrentReport = async (projectId, userId, currentDate) => {
  const currentMonth = dateUtil.getMonth(currentDate);
  const currentYear = currentDate.getFullYear();

  const reports = await Report.find({ year: currentYear, month: currentMonth, 'project.projectId': projectId, 'assignee.userId': userId });

  if (reports.length === 0) {
    let currentReport = await this.createNewCurrentReport(currentDate, currentYear, currentMonth, projectId, userId);
    console.log('Current report ' + currentReport + ' is created for project ' + projectId + ' and user ' + userId + ' in period ' + + currentMonth + '/' + currentYear);
    return currentReport;
  }

  const foundReport = reports[0];
  const currentWeek = await this.getCurrentWeek(foundReport.weeks);

  console.log('Current report ' + foundReport + ' is found for project ' + projectId + ' and user ' + userId + ' in period ' + + currentMonth + '/' + currentYear);

  return {
    _id: foundReport._id,
    year: foundReport.year,
    month: foundReport.month,
    day: currentDate.getDate(),
    week: {
      _id: currentWeek._id,
      weekNumber: currentWeek.weekNumber,
      startDate: currentWeek.startDate,
      endDate: currentWeek.endDate,
      weeklySpentTime: currentWeek.weeklySpentTime
    },
    montlySpentTime: foundReport.montlySpentTime,
    dailySpentTime: foundReport.dailySpentTime[currentDate.getDate() - 1],
    maxMontlySpentTime: foundReport.maxMontlySpentTime, // TODO should be changed to mountly max hours
    maxWeeklySpentTime: foundReport.maxWeeklySpentTime,
    maxDailySpentTime: foundReport.maxDailySpentTime
  };
}

module.exports.logTime = async (reportId, spentTime, currentDate) => {
  const existingReport = await this.findById(reportId);
  const currentWeek = await this.getCurrentWeek(existingReport.weeks);

  const updatedDailySpentTime = existingReport.dailySpentTime;
  updatedDailySpentTime[currentDate.getDate() - 1] = parseInt(existingReport.dailySpentTime[currentDate.getDate() - 1]) + parseInt(spentTime);

  // TODO update the task too

  await Report.findOneAndUpdate(
    { _id: reportId, weeks: { $elemMatch: { _id: currentWeek._id } } },
    {
      $set: {
        dailySpentTime: updatedDailySpentTime,
        'weeks.$.weeklySpentTime': currentWeek.weeklySpentTime + parseInt(spentTime),
        montlySpentTime: existingReport.montlySpentTime + parseInt(spentTime)
      }
    }
  );

}

module.exports.sendRemindingEmail = async (report) => {
  console.log(report);
  const subject = `${mailConfig.REMINDER_MAIL_SUBJECT}`;
  const content = `<h1>Time Tracking Reminder</h1><h4>Your goals are not achived yet. Please log your time.</h4><p>Your Daily Progress - ${report.dailySpentTime}/${report.maxDailySpentTime}</p><p>Your Weekly Progress - ${report.week.weeklySpentTime}/${report.maxWeeklySpentTime}</p><p>Your Montly Progress - ${report.montlySpentTime}/${report.maxMontlySpentTime}</p>`;
  mailService.sendEmail(report.assignee.email, subject, content);
}

module.exports.findById = async (id) => {
  return await Report.findById(id).populate('weeks.week');
}

module.exports.findReportDetails = async (id) => {
  return await Report.findById(id).populate('assignee.userId');
}

module.exports.searchReports = async (year, month, projectId, userId) => {
  let query = {};

  if (year) {
    query["year"] = year;
  }

  if (month) {
    query["month"] = month;
  }

  if (projectId) {
    query["project.projectId"] = projectId;
  }

  if (userId) {
    query["assignee.userId"] = userId;
  }

  console.log('Current reports search query : ');
  console.log('query["year"] = ' + query["year"]);
  console.log('query["month"] = ' + query["month"]);
  console.log('query["project.projectId"] = ' + query["project.projectId"]);
  console.log('query["assignee.userId"] = ' + query["assignee.userId"]);

  return await Report.find(query);
}

module.exports.searchCurrentReports = async (projectId, userId) => {
  const currentDate = dateUtil.getCurrentUTCDate();
  const currentYear = currentDate.getFullYear();
  const currentMonth = dateUtil.getMonth(currentDate);

  const foundReports = await this.searchReports(currentYear, currentMonth, projectId, userId)

  const promises = foundReports.map(async (foundReport) => {
    const currentWeek = await this.getCurrentWeek(foundReport.weeks);
    return {
      _id: foundReport._id,
      year: foundReport.year,
      month: foundReport.month,
      day: currentDate.getDate(),
      week: {
        _id: currentWeek._id,
        weekNumber: currentWeek.weekNumber,
        startDate: currentWeek.startDate,
        endDate: currentWeek.endDate,
        weeklySpentTime: currentWeek.weeklySpentTime
      },
      project: {
        projectId: foundReport.project.projectId,
        shortName: foundReport.project.shortName
      },
      assignee: {
        userId: foundReport.assignee.userId,
        username: foundReport.assignee.username,
        email: foundReport.assignee.email
      },
      montlySpentTime: foundReport.montlySpentTime,
      dailySpentTime: foundReport.dailySpentTime[currentDate.getDate() - 1],
      maxMontlySpentTime: foundReport.maxMontlySpentTime, // TODO should be changed to mountly max hours
      maxWeeklySpentTime: foundReport.maxWeeklySpentTime,
      maxDailySpentTime: foundReport.maxDailySpentTime
    }
  });

  return Promise.all(promises);
}

module.exports.generatePdfReport = async (report, filePath) => {
  const pdfDocument = new PDFDocument();
  pdfDocument.pipe(fs.createWriteStream(filePath));

  if (report.assignee.userId.imagePath) {
    pdfDocument.image(report.assignee.userId.imagePath.replace('http://localhost:3000/', ''), 80, 90, { width: 130 })

    pdfDocument
      .font('Helvetica')
      .fontSize(20)
      .text("Report - " + report.year + " " + report.month, 80, 60)
      .fontSize(13)
      .font('Helvetica-Bold').text("Project: ", 230, 100, { continued: true })
      .font('Helvetica').text(report.project.shortName)
      .font('Helvetica-Bold').text("User: ", 230, 120, { continued: true })
      .font('Helvetica').text(report.assignee.userId.firstName + ' ' + report.assignee.userId.lastName)
      .font('Helvetica-Bold').text("Email: ", 230, 140, { continued: true })
      .font('Helvetica').text(report.assignee.userId.email)
      .font('Helvetica-Bold').text("Skype: ", 230, 160, { continued: true })
      .font('Helvetica').text(report.assignee.userId.skype)
      .font('Helvetica-Bold').text("Phone Number: ", 230, 180, { continued: true })
      .font('Helvetica').text(report.assignee.userId.phone)
      .lineTo(110, 110);
  } else {
    pdfDocument
      .font('Helvetica')
      .fontSize(20)
      .text("Report - " + report.year + " " + report.month, 80, 60)
      .fontSize(13)
      .font('Helvetica-Bold').text("Project: ", 80, 100, { continued: true })
      .font('Helvetica').text(report.project.shortName)
      .font('Helvetica-Bold').text("User: ", 80, 120, { continued: true })
      .font('Helvetica').text(report.assignee.userId.firstName + ' ' + report.assignee.userId.lastName)
      .font('Helvetica-Bold').text("Email: ", 80, 140, { continued: true })
      .font('Helvetica').text(report.assignee.userId.email)
      .font('Helvetica-Bold').text("Skype: ", 80, 160, { continued: true })
      .font('Helvetica').text(report.assignee.userId.skype)
      .font('Helvetica-Bold').text("Phone Number: ", 80, 180, { continued: true })
      .font('Helvetica').text(report.assignee.userId.phone)
      .lineTo(110, 110);
  }

  pdfDocument
    .fontSize(14)
    .text('Montly Progress', 80, 220)
    .fontSize(10)
    .font('Helvetica-Bold').text('Spent Time: ', 80, 240, { continued: true })
    .font('Helvetica').text(report.montlySpentTime + ' hours')
    .font('Helvetica-Bold').text('Allowed Monthly Time: ', 80, 260, { continued: true })
    .font('Helvetica').text(report.maxMontlySpentTime + ' hours')
    .font('Helvetica-Bold').text('Percentage: ', 80, 280, { continued: true })
    .font('Helvetica').text((report.montlySpentTime * 100 / report.maxMontlySpentTime).toFixed(2) + ' %');

  pdfDocument
    .fontSize(14)
    .text('Weekly Progress', 80, 320)
    .fontSize(10)
    .text('Start Date', 80, 340)
    .text('End Date', 170, 340)
    .text('Spent Time', 260, 340)
    .text('Allowed Weekly Time', 350, 340)
    .text('Percentage', 470, 340);

  pdfDocument
    .moveTo(80, pdfDocument.y)
    .lineTo(530, pdfDocument.y)
    .lineWidth(0.5)
    .fillAndStroke('black')
    .fill('black');

  for (let i = 0; i < report.weeks.length; i++) {
    const position = 340 + (i + 1) * 18;
    const startDate = report.weeks[i].startDate;
    const endDate = report.weeks[i].endDate;
    pdfDocument
      .fontSize(10)
      .text(startDate.getUTCFullYear() + "-" + (startDate.getUTCMonth() + 1) + "-" + startDate.getUTCDate(), 80, position)
      .text(endDate.getUTCFullYear() + "-" + (endDate.getUTCMonth() + 1) + "-" + endDate.getUTCDate(), 170, position)
      .text(report.weeks[i].weeklySpentTime + ' hours', 260, position)
      .text(report.maxWeeklySpentTime + ' hours', 350, position)
      .text((report.weeks[i].weeklySpentTime * 100 / report.maxWeeklySpentTime).toFixed(2) + ' %', 470, position)
      .moveDown(1)
  }

  const weeks = [];
  let dayOfMonth = new Date(report.weeks[0].startDate).getDay();
  let lastDayOfMonth = new Date(report.weeks[report.weeks.length - 1].endDate).getDate();
  let dayIndex = 0;
  let currentDay = 1;
  let currentWeek = [undefined, undefined, undefined, undefined, undefined, undefined, undefined];
  while (dayOfMonth < 7) {
    let currentValue = report.dailySpentTime[dayIndex++];
    currentWeek[dayOfMonth] = {
      day: "0" + currentDay,
      value: currentValue
    };
    dayOfMonth++;
    currentDay++;
  }
  weeks[0] = currentWeek;
  for (dayIndex; dayIndex < lastDayOfMonth; dayIndex) {
    let currentWeek = [undefined, undefined, undefined, undefined, undefined, undefined, undefined];
    for (let weekIndex = 0; weekIndex < 7; weekIndex++) {
      let currentValue = report.dailySpentTime[dayIndex++];
      currentWeek[weekIndex] = {
        day: currentDay < 10 ? "0" + currentDay : currentDay,
        value: currentValue
      };
      currentDay++;
    }
    weeks[weeks.length] = currentWeek;
  }

  pdfDocument
    .fontSize(14)
    .text('Daily Progress', 80, 470)
    .fontSize(10)
    .font('Helvetica-Bold').text('Allowed Daily Time: ', 80, 490, { continued: true })
    .font('Helvetica').text(report.maxDailySpentTime + ' hours')
    .text('Sunday', 80, 510)
    .text('Monday', 145, 510)
    .text('Tuesday', 210, 510)
    .text('Wednesday', 275, 510)
    .text('Thursday', 340, 510)
    .text('Firday', 405, 510)
    .text('Saturday', 470, 510);

  pdfDocument
    .moveTo(80, pdfDocument.y)
    .lineTo(530, pdfDocument.y)
    .lineWidth(0.5)
    .fillAndStroke('black')
    .fill('black');

  for (let i = 0; i < report.weeks.length; i++) {
    const position = 510 + (i + 1) * 18;
    pdfDocument
      .fontSize(10)
      .text(weeks[i][0] ? weeks[i][0].value + ' (' + (weeks[i][0].value * 100 / report.maxDailySpentTime).toFixed(2)  + '%)' : '', 80, position)
      .text(weeks[i][1] ? weeks[i][1].value + ' (' + (weeks[i][1].value * 100 / report.maxDailySpentTime).toFixed(2)  + '%)' : '', 145, position)
      .text(weeks[i][2] ? weeks[i][2].value + ' (' + (weeks[i][2].value * 100 / report.maxDailySpentTime).toFixed(2)  + '%)' : '', 210, position)
      .text(weeks[i][3] ? weeks[i][3].value + ' (' + (weeks[i][3].value * 100 / report.maxDailySpentTime).toFixed(2)  + '%)' : '', 275, position)
      .text(weeks[i][4] ? weeks[i][4].value + ' (' + (weeks[i][4].value * 100 / report.maxDailySpentTime).toFixed(2)  + '%)' : '', 340, position)
      .text(weeks[i][5] ? weeks[i][5].value + ' (' + (weeks[i][5].value * 100 / report.maxDailySpentTime).toFixed(2)  + '%)' : '', 405, position)
      .text(weeks[i][6] ? weeks[i][6].value + ' (' + (weeks[i][6].value * 100 / report.maxDailySpentTime).toFixed(2)  + '%)' : '', 470, position)
      .moveDown(1)
  }

  return pdfDocument;
}
const Role = require('../../database/models/role.model');

module.exports.findByNameAndProjectId = async (name, projectId) => {
  return await Role.find({ projectId: projectId, name: name })
}

module.exports.findByUserIdAndProjectId = async (userId, projectId) => {
  return await Role.find({ projectId: projectId, 'user.userId': userId });
}

module.exports.findByName = async (name) => {
  return await Role.find({ name: name })
}

module.exports.create = async (role) => {
  const roleModel = new Role({
    user: role.user,
    projectId: role.projectId,
    name: role.name,
  });
  return await roleModel.save();
}

module.exports.delete = async (id) => {
  await Role.findByIdAndRemove(id);
}
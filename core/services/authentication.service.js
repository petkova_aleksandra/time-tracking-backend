const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../../database/models/user.model');
const Role = require('../../database/models/role.enum');

const ServiceError = require('../../api/errors/service.error');

const authenticationConfig = require('../../api/configs/authentication.config');
const mailConfig = require('../configs/mail.config');
const mailService = require('../services/mail.service');

module.exports.signup = async (firstName, lastName, username, email, password) => {
  const existingEmail = await User.findOne({ email: email });
  const existingUsername = await User.findOne({ username: username });

  if (existingEmail) {
    throw new ServiceError('E-mail already exists.');
  }

  if (existingUsername) {
    throw new ServiceError('Username already exists.');
  }

  // TODO use transactions 
  const hashedPassword = await bcrypt.hash(password, 12);
  const user = new User({
    firstName: firstName,
    lastName: lastName,
    username: username,
    email: email,
    password: hashedPassword,
    roles: [{ name: Role.USER }]
  });

  try {
    jwt.sign(
      { userId: user._id, roles: user.roles },
      `${authenticationConfig.SECRET}`,
      { expiresIn: `${authenticationConfig.EXPIRES_IN}` },
      (error, emailToken) => {
        const subject = `${mailConfig.CONFIRMATION_MAIL_SUBJECT}`;
        const url = `${mailConfig.CONFIRMATION_MAIL_URL}${emailToken}`
        const content = `<h1>Welcome to Time Tracking Application!</h1><h4>By clicking on the following button, you are confirming your email address.</h4><input type="button" style="width: 200px;height: 50px; background-color: #3f51b5; color: white; border: 1px solid #3f51b5;" onclick="window.open('${url}','_blank');" value="Confirm Email Address" />`;
        mailService.sendEmail(email, subject, content);
      }
    );
  } catch (error) {
    console.log('Error occured while sending confirmation email.');
    next(error);
  }
  await user.save();
  // TODO use transactions 
}

module.exports.login = async (usernameOrEmail, password) => {
  let user = await User.findOne({ $or: [{ username: usernameOrEmail }, { email: usernameOrEmail }] });
  if (!user) {
    throw new ServiceError('User does not exists.');
  }

  if (!user.confirmed) {
    throw new ServiceError('Please confirm your email.');
  }

  let isEqual = await bcrypt.compare(password, user.password);
  if (!isEqual) {
    throw new ServiceError('Password is incorect.');
  }

  const token = jwt.sign(
    { userId: user._id, roles: user.roles },
    `${authenticationConfig.SECRET}`,
    { expiresIn: `${authenticationConfig.EXPIRES_IN}` }
  );

  return {
    _id: user._id,
    firstName: user.firstName,
    lastName: user.lastName,
    username: user.username,
    email: user.email,
    roles: user.roles,
    token: token
  };
}

module.exports.confirmEmail = async (token) => {
  const decoded = jwt.verify(token, `${authenticationConfig.SECRET}`);
  await User.findOneAndUpdate({ _id: decoded.userId }, { confirmed: true });
}
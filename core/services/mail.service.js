const mailConfig = require('../configs/mail.config');

const nodeMailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');

const transport = nodeMailer.createTransport(sendgridTransport({
  auth: { api_key: `${mailConfig.SENDBOX_API_KEY}` }
}));

module.exports.sendEmail = (email, subject, content) => {
  transport.sendMail({
    to: email,
    from: `${mailConfig.SENDBOX_MAIL_SENDER}`,
    subject: subject,
    html: content
  }, function (error, info) {
    if (error) {
      console.log('Error occurred while sending email to ' + email + ', subject ' + subject + '. Error' + error);
    } else {
      console.log('Email sent successfully. Recepient ' + email + ', subject ' + subject);
    }
  });
}
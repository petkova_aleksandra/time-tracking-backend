const User = require('../../database/models/user.model');
const Task = require('../../database/models/task.model');
const Project = require('../../database/models/project.model');

const Role = require('../../database/models/role.enum');

const fs = require('fs');
const path = require('path');

// TODO use soft delete, so you can search by assignee name in task search

module.exports.findAll = async (projectId, name) => {
  if (projectId && name) {
    return await User.find({ roles: { $elemMatch: { name: name, projectId: projectId } } });
  }
  if (name) {
    return await User.find({ roles: { $elemMatch: { name: name } } });
  }
  return await User.find();
}

module.exports.findById = async (id) => {
  return await User.findById(id).populate('currentProjects');
}

module.exports.deletePreviousProfileImage = async (id) => {
  const user = await User.findById(id);
  const imagePath = user.imagePath;

  if(imagePath) {
    const parts = imagePath.split('/');
    const location = path.join(__dirname, '../../images/' + parts[parts.length-1]);
    console.log('Deleting profile image on location : ' + location);
    fs.unlinkSync(location);
    console.log('Deleted profile image image on location : ' + location);
  }
}

module.exports.update = async (id, user) => {
  await User.findOneAndUpdate({ _id: id }, user);
}

module.exports.findUserProjects = async (id) => {
  const user = await User.findById(id).populate('currentProjects');
  return user.currentProjects;
}

module.exports.findUnassignedProjectAdmins = async (projectId) => {
  return await User.find({
    currentProjects: projectId,
    roles: { $elemMatch: { name: { $ne: Role.PROJECT_ADMIN }, projectId: { $ne: projectId } } }
  });
}

module.exports.findUnassignedProjectAdmins = async (projectId) => {
  const projectAdmins = await this.findProjectAdmins(projectId);
  const userIds = projectAdmins.map(user => user._id.toString());
  const project = await Project.findById(projectId);
  return project.users.filter(user => !userIds.includes(user.userId.toString()));
}

module.exports.findProjectAdmins = async (projectId) => {
  return await User.find({
    roles: { $elemMatch: { name: Role.PROJECT_ADMIN, projectId: projectId } }
  });
}

module.exports.findCoworkers = async (id) => {
  let coworkers = [];
  const projects = await this.findUserProjects(id);

  if (projects) {
    projects.forEach(project => coworkers.push(...project.users));
  }

  coworkers = coworkers.reduce((unique, coworkerToCompare) => {
    if (!unique.some(coworker => coworker.username === coworkerToCompare.username)) {
      unique.push(coworkerToCompare);
    }
    return unique;
  }, []);

  console.log('User ' + id + ' has coworkers ' + coworkers.map(coworker => coworker.username));

  return coworkers;
}


module.exports.findCoworkersTasks = async (userId, identifier, title, projectId, assigneeId, status, priority) => {
  let query = {};

  if (identifier) {
    let identifierParts = identifier.split('-');
    query["project.shortName"] = identifierParts[0];
    query["identifier"] = identifierParts[1];
  }

  if (title) {
    query["title"] = title;
  }

  if (projectId) {
    query["project.projectId"] = projectId;
  } else {
    let projectIds = [];
    const projects = await this.findUserProjects(userId);
    projects.forEach(project => projectIds.push(project._id));
    query["project.projectId"] = { $in: projectIds };
  }

  if (assigneeId) {
    query["assignee.userId"] = assigneeId;
  }

  if (status) {
    query["status"] = status;
  }

  if (priority) {
    query["priority"] = priority;
  }

  console.log('Coworkers tasks search query : ');
  console.log('query["project.shortName"] = ' + query["project.shortName"]);
  console.log('query["identifier"] = ' + query["identifier"]);
  console.log('query["title"] = ' + query["title"]);
  console.log('query["project.projectId"] = ' + query["project.projectId"]);
  console.log('query["assignee.userId"] = ' + query["assignee.userId"]);
  console.log('query["status"] = ' + query["status"]);
  console.log('query["priority"] = ' + query["priority"]);

  return await Task.find(query);
}

module.exports.create = async (role) => {
  await User.find(
    { _id: projectId, users: { $elemMatch: { userId: userId } } },
    { $set: { 'users.$.maxDayHours': maxDayHours, 'users.$.maxWeekHours': maxWeekHours } }
  );
  const roleModel = new Role({
    user: role.user,
    projectId: role.projectId,
    name: role.name,
  });
  return await roleModel.save();
}

module.exports.delete = async (id) => {
  await Role.findByIdAndRemove(id);
}

module.exports.addUserRole = async (id, role) => {
  console.log(role.name + '-' + role.projectId)
  await User.updateOne({ _id: id }, { $push: { roles: role } });
}

module.exports.deleteUserRole = async (userId, role) => {
  await User.updateOne({ _id: userId }, { $pull: { roles: role } });
} 
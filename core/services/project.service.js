const mongoose = require('mongoose');

const roleService = require('../services/role.service');

const Project = require('../../database/models/project.model');
const User = require('../../database/models/user.model');

const Role = require('../../database/models/role.enum');

module.exports.create = async (project) => {
  const projectModel = new Project({
    shortName: project.shortName,
    fullName: project.fullName,
    description: project.description,
  });
  return await projectModel.save(projectModel);
}

module.exports.update = async (id, project) => {
  await Project.findOneAndUpdate({ _id: id }, project);
}

// TODO use soft delete, so you can search by project name in task search
module.exports.delete = async (id) => {
  await Project.findByIdAndRemove(id);
}

module.exports.findAll = async () => {
  return await Project.find();
}

module.exports.findById = async (id) => {
  return await Project.findById(id);
}

module.exports.findProjectUnassignedUsers = async (id) => {
  const project = await Project.findById(id);
  const userIds = project.users.map(user => user.userId);
  console.log('Found project assigned users ' + userIds + ' to project ' + id);
  return await User.find({ _id: { $nin: userIds } });
}

// TODO check for better solution without fetching the project
module.exports.addUserToProject = async (userId, projectId, maxDayHours, maxWeekHours, maxMonthHours) => {
  const user = await User.findById(userId);
  const project = await Project.findById(projectId);
  const projectUser = {
    userId: userId,
    username: user.username,
    email: user.email,
    maxDayHours: maxDayHours,
    maxWeekHours: maxWeekHours,
    maxMonthHours: maxMonthHours
  };

  // TODO transaction
  await Project.updateOne(
    { _id: projectId },
    { $push: { users: projectUser } }
  );
  await User.updateOne(
    { _id: userId },
    { $push: { currentProjects: project } }
  );
  // TODO transaction
}

module.exports.updateProjectUser = async (userId, projectId, maxDayHours, maxWeekHours, maxMonthHours) => {
  await Project.findOneAndUpdate(
    { _id: projectId, users: { $elemMatch: { userId: userId } } },
    { $set: { 'users.$.maxDayHours': maxDayHours, 'users.$.maxWeekHours': maxWeekHours, 'users.$.maxMonthHours': maxMonthHours } }
  );
}

// TODO check for better solution without fetching the project
module.exports.deleteUserFromProject = async (userId, projectId) => {
  const project = await Project.findById(projectId);
  const projectUser = {
    userId: userId
  };

  // TODO transaction
  await Project.updateOne(
    { _id: projectId },
    { $pull: { users: projectUser, roles: { name: Role.PROJECT_ADMIN, projectId: projectId } }, }
  );
  await User.updateOne(
    { _id: userId },
    { $pull: { currentProjects: project._id } } // TODO remove project admin role
  );
  // TODO transaction
}
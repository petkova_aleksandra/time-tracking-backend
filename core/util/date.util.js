module.exports.MONTHS = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST",
  "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"];

module.exports.getCurrentServerDate = () => {
  const serverTime = new Date();
  return this.toUTCDate(new Date(serverTime.getFullYear(), serverTime.getMonth(), serverTime.getDate()));
}

module.exports.getCurrentUTCDate = () => {
  const serverTime = new Date();
  const serverTimeOffset = serverTime.getTimezoneOffset() * 60 * 1000;
  return new Date(serverTime.getTime() - serverTimeOffset);
}

module.exports.toUTCDate = (serverTime) => {
  const serverTimeOffset = serverTime.getTimezoneOffset() * 60 * 1000;
  return new Date(serverTime.getTime() - serverTimeOffset);
}

module.exports.getMonth = (date) => {
  return this.MONTHS[date.getMonth()];
}

module.exports.getNumberOfDaysInMonth = function (date) {
  // Here January is 1 based
  // Day 0 is the last day in the previous month
  return this.toUTCDate(new Date(date.getFullYear(), date.getMonth(), 0)).getDate() + 1;
  // Here January is 0 based
  // return new Date(year, month+1, 0).getDate();
};

module.exports.getLastDayOfMonth = function (date) {
  return this.toUTCDate(new Date(date.getFullYear(), date.getMonth() + 1, 0)).getDate();
}

module.exports.getFirstDateOfMonth = function (date) {
  return this.toUTCDate(new Date(date.getFullYear(), date.getMonth(), 1));
}

module.exports.getLastDateOfMonth = function (date) {
  return this.toUTCDate(new Date(date.getFullYear(), date.getMonth() + 1, 0));
}
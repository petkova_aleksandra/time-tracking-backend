const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const mongodbConfig = require('./database/configs/mongodb.config');

const authenticationRoutes = require('./api/routes/authentication.route');
const userRoutes = require('./api/routes/user.route');
const roleRoutes = require('./api/routes/role.route');
const projectRoutes = require('./api/routes/project.route');
const taskRoutes = require('./api/routes/task.route');
const reportRoutes = require('./api/routes/report.route');

const errorHandler = require('./api/middlewares/error-handler.middleware');

mongoose
  .connect(
    `mongodb+srv://${mongodbConfig.USERNAME}:${mongodbConfig.PASSWORD}@${mongodbConfig.HOST}/${mongodbConfig.DATABASE}?retryWrites=true&w=majority`,
    { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false }
  )
  .then(() => {
    console.log("Connected to mongodb successfully.");
  })
  .catch(error => {
    console.log("Error occured while connecting to mongodb. Error: " + error);
  });

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/images', express.static(__dirname + '/images'));

app.use(cors());
  
app.use("/api", authenticationRoutes);
app.use("/api/projects", projectRoutes);
app.use("/api/tasks", taskRoutes);
app.use("/api/users", userRoutes, authenticationRoutes);
app.use("/api/roles", roleRoutes);
app.use("/api/reports", reportRoutes);

app.use(errorHandler);
  
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`)
});
  

const authenticationService = require('../../core/services/authentication.service');

module.exports.signup = (req, res, next) => {
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password;

  console.log('Received request for signing up user with username ' + username + ' and email ' + email);

  authenticationService
    .signup(firstName, lastName, username, email, password)
    .then(() => {
      console.log('User with username ' + username + ' and email ' + email + ' is signed up successfully.')
      return res.status(200).json({
        message: 'User is signed up.'
      });
    })
    .catch(error => {
      console.error('Error occured while signing up user with username ' + username + ' and email ' + email
        + '. Error: ' + error);
      next(error);
    });
}

module.exports.login = (req, res, next) => {
  const usernameOrEmail = req.body.usernameOrEmail;
  const password = req.body.password;

  console.log('Received request for logging user with username/email ' + usernameOrEmail);

  authenticationService
    .login(usernameOrEmail, password)
    .then(user => {
      console.log('User with username/email ' + usernameOrEmail + ' logged in successfully.');
      return res.status(200).json({
        message: 'User is logged in.',
        user: user,
        expiresIn: 3600
      });
    })
    .catch(error => {
      console.error('Error occured while logging user with username/email ' + usernameOrEmail + '. ' + error);
      next(error);
    });
}

module.exports.confirmEmail = (req, res, next) => {
  const token = req.body.token;

  console.log('Received request for confirming email for token ' + token);

  authenticationService
    .confirmEmail(token)
    .then(() => {
      console.log('Email confirmed for token ' + token);
      return res.status(200).json('Your email address is confirmed.');
    })
    .catch(error => {
      console.error('Error occured while confirming email with token ' + token + '. Error: ' + error);
      next(error);
    });
}
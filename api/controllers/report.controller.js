const reportService = require('../../core/services/report.service');

const dateUtil = require('../../core/util/date.util');

const fs = require('fs');
const path = require('path');

module.exports.findCurrentReport = (req, res, next) => {
  const projectId = req.query.projectId;
  const userId = req.query.userId;
  const currentDate = dateUtil.getCurrentUTCDate();

  console.log('Received request for retrieving current report with project ' + projectId + ', assignee ' + userId + ' for date ' + currentDate);

  reportService
    .findCurrentReport(projectId, userId, currentDate)
    .then(report => {
      console.log('Current report is retrieved successfully.');
      return res.status(200).json({
        message: 'Current report is retrieved.',
        report: report
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving current report with project ' + projectId + ', assignee ' + userId + ' for date ' + currentDate + '. Error: ' + error);
      next(error);
    });
}

module.exports.logTime = (req, res, next) => {
  const id = req.params.id;
  const spentTime = req.body.spentTime;
  const currentDate = dateUtil.getCurrentUTCDate();

  console.log('Received request for logging time for current report ' + id + ', spentTime ' + spentTime);

  reportService
    .logTime(id, spentTime, currentDate)
    .then(report => {
      console.log('Time logged successfully.');
      return res.status(200).json({
        message: 'Time logged successfully.',
        report: report
      });
    })
    .catch(error => {
      console.log('Error occured while logging time for for current report ' + id + ', spentTime ' + spentTime + '. Error: ' + error);
      next(error);
    });
}

module.exports.sendRemindingEmail = (req, res, next) => {
  const id = req.params.id;
  const report = req.body.report;

  console.log('Received request for sending reminding email for report ' + id);

  reportService
    .sendRemindingEmail(report)
    .then(report => {
      console.log('Reminding email sent for report ' + id);
      return res.status(200).json({
        message: 'Reminding email sent.',
        report: report
      });
    })
    .catch(error => {
      console.log('Error occured while sending reminging email for report ' + id + '. Error: ' + error);
      next(error);
    });
}

module.exports.searchCurrentReports = (req, res, next) => {
  const projectId = req.query.projectId;
  const userId = req.query.userId;

  console.log('Received request for searching current reports with project ' + projectId + ', user ' + userId);

  reportService
    .searchCurrentReports(projectId, userId)
    .then(reports => {
      console.log('Current reports are retrieved successfully.');
      return res.status(200).json({
        message: 'Current reports are retrieved.',
        reports: reports
      });
    })
    .catch(error => {
      console.log('Error occured while searching current reports with project ' + projectId + ', user ' + userId + '. Error: ' + error);
      next(error);
    });
}

module.exports.searchPastReports = (req, res, next) => {
  const year = req.query.year;
  const month = req.query.month;
  const projectId = req.query.projectId;
  const userId = req.query.userId;

  console.log('Received request for searching past reports with project ' + projectId + ', user ' + userId + ', year ' + year + ', month ' + month);

  reportService
    .searchReports(year, month, projectId, userId)
    .then(reports => {
      console.log('Past reports are retrieved successfully.');
      return res.status(200).json({
        message: 'Past reports are retrieved.',
        reports: reports
      });
    })
    .catch(error => {
      console.log('Error occured while searching past reports with project ' + projectId + ', user ' + userId + ', year ' + year + ', month ' + month + '. Error: ' + error);
      next(error);
    });
}


module.exports.getPdf = async (req, res, next) => {
  const reportId = req.params.id;

  console.log('Received request for retrieving pdf for report ' + reportId);

  const reportName = 'report-' + reportId + '.pdf';
  const filePath = path.join('pdfs/', reportName);

  reportService
    .findReportDetails(reportId)
    .then(report => {
      console.log('Started generating pdf report' + reportId);
      return reportService.generatePdfReport(report, filePath);
    })
    .then(pdfDocument => {
      console.log('Started streaming pdf report' + reportId);
      
      pdfDocument.pipe(res);
      pdfDocument.end();

      const file = fs.createReadStream(filePath);
      res.setHeader('Content-Type', 'application/pdf');
      res.setHeader('Content-Disposition', 'inline; filename="' + reportName + '"');
      file.pipe(res);
    })
    .catch(error => {
      console.log('Error occured while retrieving pdf for report ' + reportId + '. Error: ' + error);
      next(error);
    });
}
const userService = require('../../core/services/user.service');

const User = require('../../database/models/user.model');

// TODO do not return user with pass
module.exports.findAll = (req, res, next) => {
  let projectId = req.query.projectId;
  let name = req.query.name;

  console.log('Received request for retrieving users for role : ' + name + ', projectId : ' + projectId);

  userService
    .findAll(projectId, name)
    .then((users) => {
      console.log('Users for role : ' + name + ', projectId : ' + projectId + ' are retrieved successfully.');
      res.status(200).json({
        message: 'Users are retrieved.',
        users: users
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving the users for role : ' + name + ', projectId : ' + projectId + '. Error: ' + error);
      next(error);
    });
}

module.exports.findById = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for retrieving user ' + id);

  userService
    .findById(id)
    .then(user => {
      console.log('User ' + id + ' is retrieved successfully.')
      return res.status(200).json({
        message: 'User is retrieved.',
        user: user
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving the user. Error: ' + error);
      next(error);
    });
}

module.exports.update = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for updating user.');

  // TODO remove previous image if needed
  const url = req.protocol + '://' + req.get('host');
  const user = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    skype: req.body.skype,
    phone: req.body.phone
  };

  if (req.file) {
    user.imagePath = url + '/images/' + req.file.filename;
  }

  userService
  .deletePreviousProfileImage(id)
  .then(() => {
    console.log('Profile image is deleted for user ' + id);
  })
  .catch(error => {
    console.log('Error occured while deleting profile image for user ' + id + '. Error: ' + error);
    next(error);
  });

  userService
    .update(id, user)
    .then(() => {
      console.log('User ' + id + ' is updated successfully.');
      return res.status(200).json({
        message: 'User is updated.'
      });
    })
    .catch(error => {
      console.log('Error occured while updating the user ' + id + '. Error: ' + error);
      next(error);
    });
}

module.exports.findUserProjects = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for retrieving user projects for user ' + id);

  userService
    .findUserProjects(id)
    .then(projects => {
      console.log('User projects for user ' + id + ' are retrieved successfully.');
      res.status(200).json({
        message: 'User projects are retrieved.',
        projects: projects
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving the user projects for user ' + id + '. Error: ' + error);
      next(error);
    });
}

module.exports.findCoworkers = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for retrieving user coworkers for user ' + id);

  userService
    .findCoworkers(id)
    .then(users => {
      console.log('User coworkers for user ' + id + ' are retrieved successfully.');
      res.status(200).json({
        message: 'User coworkers are retrieved.',
        users: users
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving the user coworkers for user ' + id + '. Error: ' + error);
      next(error);
    });
}

module.exports.findCoworkersTasks = (req, res, next) => {
  let userId = req.params.id;
  const identifier = req.query.identifier;
  const title = req.query.title;
  const projectId = req.query.projectId;
  const assigneeId = req.query.assigneeId;
  const status = req.query.status;
  const priority = req.query.priority;

  console.log('Received request for retrieving user coworkers tasks for user ' + userId + ' with identifier ' + identifier
    + ', title ' + title + ', project ' + projectId + ', assignee ' + assigneeId + ', status ' + status + ' and priority ' + priority);

  userService
    .findCoworkersTasks(userId, identifier, title, projectId, assigneeId, status, priority)
    .then(tasks => {
      console.log('User coworkers tasks for user ' + userId + ' are retrieved successfully.');
      res.status(200).json({
        message: 'User coworkers tasks are retrieved.',
        tasks: tasks
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving the user coworkers tasks for user ' + userId + ' with identifier ' + identifier
        + ', title ' + title + ', project ' + projectId + ', assignee ' + assigneeId + ', status ' + status + ' and priority '
        + priority + '. Error: ' + error);
      next(error);
    });
}

module.exports.addUserRole = (req, res, next) => {
  let id = req.params.id;
  let role = req.body;

  console.log('Received request for adding role ' + role + ' to user ' + id);

  userService
    .addUserRole(id, role)
    .then(users => {
      console.log('Role ' + role + ' added to user ' + id + ' successfully.');
      res.status(200).json({
        message: 'Role added.',
        users: users
      });
    })
    .catch(error => {
      console.log('Error occured while adding role ' + role + ' to user ' + id + '. Error: ' + error);
      next(error);
    });
}

module.exports.deleteUserRole = (req, res, next) => {
  let userId = req.params.userId;
  let role = {
    _id: req.params.roleId,
    name: req.query.name,
    projectId: req.query.projectId
  };

  console.log('Received request for deleting role ' + role.name + ' to user ' + userId);

  userService
    .deleteUserRole(userId, role)
    .then(users => {
      console.log('Role ' + role.name + ' deleted to user ' + userId + ' successfully.');
      res.status(200).json({
        message: 'Role deleted.',
        users: users
      });
    })
    .catch(error => {
      console.log('Error occured while deleting role ' + role.name + ' to user ' + userId + '. Error: ' + error);
      next(error);
    });
}
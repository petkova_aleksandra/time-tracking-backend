const projectService = require('../../core/services/project.service');
const userService = require('../../core/services/user.service');

module.exports.create = (req, res, next) => {
  console.log('Received request for creating project.');

  projectService
    .create(req.body)
    .then(project => {
      console.log('Project ' + project._id + ' is created successfully.');
      return res.status(200).json({
        message: 'Project is created'
      });
    })
    .catch(error => {
      console.log('Error occured while creating the project. Error: ' + error);
      next(error);
    });
}

module.exports.update = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for updating project.');

  projectService
    .update(id, req.body)
    .then(() => {
      console.log('Project ' + id + ' is updated successfully.');
      return res.status(200).json({
        message: 'Project is updated.'
      });
    })
    .catch(error => {
      console.log('Error occured while updating the project ' + id + '. Error: ' + error);
      next(error);
    });
}

module.exports.delete = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for deleting project ' + id);

  projectService
    .delete(id)
    .then(() => {
      console.log('Project ' + id + 'is deleted successfully.');
      return res.status(200).json({
        message: 'Project is deleted.'
      });
    })
    .catch(error => {
      console.log('Error occured while deleting the project ' + id + '. Error: ' + error);
      next(error);
    });
}

module.exports.findAll = (req, res, next) => {
  console.log('Received request for retrieving projects.');

  projectService
  .findAll()
    .then(projects => {
      console.log('Projects are retrieved successfully.');
      return res.status(200).json({
        message: 'Projects are retrieved.',
        projects: projects
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving the projects. Error: ' + error);
      next(error);
    });
}

module.exports.findById = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for retrieving project.');

  projectService
    .findById(id)
    .then(project => {
      console.log('Project ' + id + ' is retrieved successfully.')
      return res.status(200).json({
        message: 'Project is retrieved.',
        project: project
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving the project. Error: ' + error);
      next(error);
    });
}

module.exports.findProjectUnassignedUsers = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for retrieving project unassigned users for project ' + id);

  projectService
    .findProjectUnassignedUsers(id)
    .then((users) => {
      console.log('Project unassigned users for project ' + id + ' are retrieved successfully.');
      res.status(200).json({
        message: 'User project unassigned users are retrieved.',
        users: users
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving the project unassigned users for project ' + id + '. Error: ' + error);
      next(error);
    });
}

module.exports.addUserToProject = (req, res, next) => {
  let userId = req.params.userId;
  let projectId = req.params.projectId;
  let maxDayHours = req.body.maxDayHours;
  let maxWeekHours = req.body.maxWeekHours;
  let maxMonthHours = req.body.maxMonthHours;

  console.log('Received request for adding user ' + userId + ' to project ' + projectId);

  projectService
    .addUserToProject(userId, projectId, maxDayHours, maxWeekHours, maxMonthHours)
    .then(() => {
      console.log('User ' + userId + ' is added to project ' + projectId + ' successfully.');
      return res.status(200).json({
        message: 'User is added to the project.'
      });
    })
    .catch(error => {
      console.log('Error occured while adding user ' + userId + ' to project ' + projectId + '. Error: ' + error);
      next(error);
    });
}

module.exports.updateProjectUser = (req, res, next) => {
  let userId = req.params.userId;
  let projectId = req.params.projectId;
  let maxDayHours = req.body.maxDayHours;
  let maxWeekHours = req.body.maxWeekHours;
  let maxMonthHours = req.body.maxMonthHours;

  console.log('Received request for updating project user ' + userId + ', project ' + projectId);

  projectService
    .updateProjectUser(userId, projectId, maxDayHours, maxWeekHours, maxMonthHours)
    .then(() => {
      console.log('Updating project user ' + userId + ', project ' + projectId + ' is updated successfully.');
      return res.status(200).json({
        message: 'Project user is updated.'
      });
    })
    .catch(error => {
      console.log('Error occured while updating project user ' + userId + ', project ' + projectId + '. Error: ' + error);
      next(error);
    });
}

module.exports.deleteUserFromProject = (req, res, next) => {
  let userId = req.params.userId;
  let projectId = req.params.projectId;

  console.log('Received request for deleting user ' + userId + ' from project ' + projectId);

  projectService
    .deleteUserFromProject(userId, projectId)
    .then(() => {
      console.log('User ' + userId + ' is deleted from project ' + projectId + ' successfully.');
      return res.status(200).json({
        message: 'User is deleted from the project.'
      });
    })
    .catch(error => {
      console.log('Error occured while deleting user ' + userId + ' from project ' + projectId + '. Error: ' + error);
      next(error);
    });
}

module.exports.findUnassignedProjectAdmins = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for retrieving unassigned project admins for project ' + id);

  userService
    .findUnassignedProjectAdmins(id)
    .then(users => {
      console.log('Unassigned project admins for project ' + id + ' are retrieved successfully.');
      res.status(200).json({
        message: 'Unassigned project admins are retrieved.',
        users: users
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving Unassigned project admins for project ' + id + '. Error: ' + error);
      next(error);
    });
}
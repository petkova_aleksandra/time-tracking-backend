const roleService = require('../../core/services/role.service');

module.exports.findByName = (req, res, next) => {
  let projectId = req.query.projectId;
  let name = req.params.name;

  if (projectId) {
    console.log('Received request for retrieving roles ' + name + ' for project ' + projectId);
    roleService
      .findByNameAndProjectId(name, projectId)
      .then(roles => {
        console.log('Roles ' + name + ' for project ' + projectId + ' are retrieved successfully.');
        res.status(200).json({
          message: 'Roles are retrieved.',
          roles: roles
        });
      })
      .catch(error => {
        console.log('Error occured while retrieving roles ' + name + ' for project ' + projectId + ' Error: ' + error);
        next(error);
      });
  } else {
    console.log('Received request for retrieving roles ' + name);
    roleService
      .findByName(name)
      .then(roles => {
        console.log('Roles ' + name + ' are retrieved successfully.');
        res.status(200).json({
          message: 'Roles are retrieved.',
          roles: roles
        });
      })
      .catch(error => {
        console.log('Error occured while retrieving roles ' + name + ' Error: ' + error);
        next(error);
      });
  }
}

module.exports.create = (req, res, next) => {
  const userId = req.body.user.userId;
  const projectId = req.body.projectId;
  const name = req.body.name;

  console.log('Received request for creating role ' + name + ' for user ' + userId + ' and project ' + projectId);

  roleService
    .create(req.body)
    .then(role => {
      console.log('Role ' + name + ' with id ' + role._id + ' for user ' + userId + ' and project ' + projectId
        + ' is created successfully.');
      return res.status(200).json({
        message: 'Role is created.'
      });
    })
    .catch(error => {
      console.log('Error occured while creating the role ' + name + ' for user ' + userId + ' and project ' + projectId
        + '. Error: ' + error);
      next(error);
    });
}

module.exports.delete = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for deleting role ' + id);

  roleService
    .delete(id)
    .then(() => {
      console.log('Role ' + id + 'is deleted successfully.');
      return res.status(200).json({
        message: 'Role is deleted.'
      });
    })
    .catch(error => {
      console.log('Error occured while deleting the role ' + id + '. Error: ' + error);
      next(error);
    });
}
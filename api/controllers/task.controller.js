const taskService = require('../../core/services/task.service');

module.exports.create = (req, res, next) => {
  const assigneeId = req.body.assignee._id;
  const projectId = req.body.project._id;

  console.log('Received request for creating task for assignee ' + assigneeId + ' and project ' + projectId);

  taskService
    .create(req.body)
    .then(task => {
      console.log('Task ' + task._id + ' is created successfully.');
      return res.status(200).json({
        message: 'Task ' + task.project.shortName + '-' + task.identifier + ' is created.'
      });
    })
    .catch(error => {
      console.log('Error occured while creating the task for assignee ' + assigneeId + ' and project ' + projectId + '. Error: ' + error);
      next(error);
    });
}

module.exports.update = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for updating task.');

  taskService
    .update(id, req.body)
    .then(() => {
      console.log('Task ' + id + ' is updated successfully.');
      return res.status(200).json({
        message: 'Task is updated.'
      });
    })
    .catch(error => {
      console.log('Error occured while updating the task ' + id + '. Error: ' + error);
      next(error);
    });
}

module.exports.delete = (req, res, next) => {
  let id = req.params.id;

  console.log('Received request for deleting task ' + id);

  taskService
    .delete(id)
    .then(() => {
      console.log('Task ' + id + 'is deleted successfully.');
      return res.status(200).json({
        message: 'Task is deleted.'
      });
    })
    .catch(error => {
      console.log('Error occured while deleting the task ' + id + '. Error: ' + error);
      next(error);
    });
}

module.exports.findAll = (req, res, next) => {
  const identifier = req.query.identifier;
  const title = req.query.title;
  const projectId = req.query.projectId;
  const assigneeId = req.query.assigneeId;
  const status = req.query.status;
  const priority = req.query.priority;

  console.log('Received request for retrieving task with identifier ' + identifier + ', title ' + title
    + ', project ' + projectId + ', assignee ' + assigneeId + ', status ' + status + ' and priority ' + priority);

  taskService
    .findAll(identifier, title, projectId, assigneeId, status, priority)
    .then(tasks => {
      console.log('Tasks are retrieved successfully.');
      return res.status(200).json({
        message: 'Tasks are retrieved.',
        tasks: tasks
      });
    })
    .catch(error => {
      console.log('Error occured while retrieving the tasks with identifier ' + identifier + ', title ' + title
        + ', project ' + projectId + ', assignee ' + assigneeId + ', status ' + status + ' and priority ' + priority + '. Error: ' + error);
      next(error);
    });
}
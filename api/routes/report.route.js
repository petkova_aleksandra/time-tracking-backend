const express = require('express');

const authenticate = require('../middlewares/authenticate.middleware');

const reportController = require('../controllers/report.controller');

const router = express.Router();

router.get('/current', authenticate, reportController.findCurrentReport);
router.get('/current/search', authenticate, reportController.searchCurrentReports);
router.get('/past/search', authenticate, reportController.searchPastReports);
router.put('/:id/logTime', authenticate, reportController.logTime);
router.put('/:id/sendRemindingEmail', authenticate, reportController.sendRemindingEmail);
router.get('/:id/pdf', reportController.getPdf);

module.exports = router;
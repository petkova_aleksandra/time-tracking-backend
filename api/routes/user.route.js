const express = require('express');

const authenticate = require('../middlewares/authenticate.middleware');

const userController = require('../controllers/user.controller');

const router = express.Router();

const multer = require('multer');

const MIME_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg'
};

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = null;
    if(!isValid) {
      error = new Error('Invalid mime type');
    }
    callback(error, 'images')
  },
  filename: (req, file, callback) => {
    const name = file.originalname.toLowerCase().split(' ').join('-');
    const extention = MIME_TYPE_MAP[file.mimetype];
    callback(null, name + '-' + Date.now() + '.' + extention);
  }
});

router.get('', authenticate, userController.findAll);
router.get('/:id', authenticate, userController.findById);
router.put('/:id', multer({storage: storage}).single("image"), authenticate, userController.update);
router.get('/:id/projects', authenticate, userController.findUserProjects);
router.get('/:id/coworkers', authenticate, userController.findCoworkers);
router.get('/:id/coworkers-tasks', authenticate, userController.findCoworkersTasks);
router.put('/:id/role', authenticate, userController.addUserRole);
router.delete('/:userId/role/:roleId', authenticate, userController.deleteUserRole);

module.exports = router;
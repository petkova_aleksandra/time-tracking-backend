const express = require('express');

const authenticate = require('../middlewares/authenticate.middleware');
const authorize = require('../middlewares/authorize.middleware');

const Role = require('../../database/models/role.enum');

const taskController = require('../controllers/task.controller');

const router = express.Router();

router.get('', authenticate, authorize(Role.ADMIN), taskController.findAll);
router.post('', authenticate, taskController.create);
router.put('/:id', authenticate, taskController.update);
router.delete('/:id', authenticate, authorize(Role.ADMIN), taskController.delete);

module.exports = router;
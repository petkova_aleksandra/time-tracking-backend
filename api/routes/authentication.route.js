const express = require('express');

const authenticationController = require('../controllers/authentication.controller');

const router = express.Router();

router.post('/signup', authenticationController.signup);
router.post('/login', authenticationController.login);
router.post('/confirmation', authenticationController.confirmEmail);
// router.put('/:id/role/project-admin', authenticate, authorize(Role.ADMIN), userController.setProjectAdminRole);

module.exports = router;
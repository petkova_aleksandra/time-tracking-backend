const express = require('express');

const authenticate = require('../middlewares/authenticate.middleware');
const authorize = require('../middlewares/authorize.middleware');

const projectController = require('../controllers/project.controller');

const Role = require('../../database/models/role.enum');

const router = express.Router();

router.get('', authenticate, projectController.findAll);
router.get('/:id', authenticate, projectController.findById);
router.post('', authenticate, authorize(Role.ADMIN, Role.PROJECT_ADMIN), projectController.create);
router.put('/:id', authenticate,authorize(Role.ADMIN, Role.PROJECT_ADMIN), projectController.update);
router.delete('/:id', authenticate, authorize(Role.ADMIN, Role.PROJECT_ADMIN), projectController.delete);
router.get('/:id/unassigned-users', authenticate, projectController.findProjectUnassignedUsers);
router.post('/:projectId/users/:userId', authenticate, authorize(Role.ADMIN, Role.PROJECT_ADMIN), projectController.addUserToProject);
router.patch('/:projectId/users/:userId', authenticate, authorize(Role.ADMIN, Role.PROJECT_ADMIN), projectController.updateProjectUser);
router.delete('/:projectId/users/:userId', authenticate, authorize(Role.ADMIN, Role.PROJECT_ADMIN), projectController.deleteUserFromProject);
router.get('/:id/unassigned-project-admins', authenticate, projectController.findUnassignedProjectAdmins);

module.exports = router;
const express = require('express');

const authenticate = require('../middlewares/authenticate.middleware');
const authorize = require('../middlewares/authorize.middleware');

const roleController = require('../controllers/role.controller');

const Role = require('../../database/models/role.enum');

const router = express.Router();

router.get('/:name', authenticate, roleController.findByName);
router.post('', authenticate, authorize(Role.ADMIN), roleController.create);
router.delete('/:id', authenticate, authorize(Role.ADMIN), roleController.delete);

module.exports = router;
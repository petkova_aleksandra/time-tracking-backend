module.exports = {
   SECRET : 'secret_this_should_be_longer',
   EXPIRES_IN : '1h',
   ALGORITHMS : ['HS256']
}
function AuthenticationError(message) {
  this.name = "AuthenticationError";
  this.message = (message || "");
}
AuthenticationError.prototype = Error.prototype;

module.exports = AuthenticationError;
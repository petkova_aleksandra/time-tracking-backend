function AuthorizationError(message) {
  this.name = "AuthorizationError";
  this.message = (message || "");
}
AuthorizationError.prototype = Error.prototype;

module.exports = AuthorizationError;
module.exports = (err, req, res, next) => {
  console.log('Error occured: ' + err);
  if (err.name === 'AuthorizationError') {
    return res.status(403).json({ 
      message: err.message
    });
  }
  if (err.name === 'AuthenticationError') {
    return res.status(401).json({ 
      message: err.message
    });
  }
  if (err.name === 'ServiceError') {
    return res.status(500).json({
      message: err.message
    });
  }
  return res.status(500).json({
    message: 'Tehcnical error occured.'
  });
}
const AuthorizationError = require('../errors/authorization.error');

module.exports = (...allowedRoles) => {
  return (req, res, next) => {
    const userRoles = req.user.roles;
    if (allowedRoles && userRoles) {
      const filteredUserRoles = userRoles.filter(userRole => allowedRoles.includes(userRole.name));
      if (filteredUserRoles && filteredUserRoles.length !== 0) {
        console.log('next');
        return next();
      }
    }

    console.log('Authorization failed for user ' + req.user.userId);
    next(new AuthorizationError('Access denied.'));
  }
}
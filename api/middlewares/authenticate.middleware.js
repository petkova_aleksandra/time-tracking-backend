const jwt = require('jsonwebtoken');
const { SECRET } = require('../configs/authentication.config');
const AuthenticationError = require('../errors/authentication.error');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, SECRET);
    req.user = { userId: decoded.userId, roles: decoded.roles };
    next();
  } catch (error) {
    console.log('Authentication failed for request url ' + req.url + '. Error: ' + error);
    next(new AuthenticationError('Access denied.'));
  }
}
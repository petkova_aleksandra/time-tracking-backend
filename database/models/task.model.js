const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const taskSchema = new Schema(
  {
    identifier: { type: String, required: true },
    title: { type: String, required: true },
    assignee: {
      userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
      username: { type: String, required: true },
      email: { type: String, required: true }
    },
    project: { 
      projectId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Project' },
      shortName: { type: String, required: true }
    },
    status: { type: String, required: true, enum: ['NEW', 'IN_PROGRESS', 'DONE', 'PENDING', 'BLOCKED'], default: 'NEW' },
    priority: { type: String, required: true, enum: ['LOW', 'MEDIUM', 'HIGH'] },
    type: { type: String },
    description: { type: String, required: true },
    estimationInHours: { type: Number },
    spentHours: { type: Number },
    startDate: { type: Date },
    endDate: { type: Date },
    dueDate: { type: Date }
  },
  { timestamps: true }
);

// TODO add indexes
// schema.index({name: 'text', 'profile.something': 'text'});

module.exports = mongoose.model('Task', taskSchema);

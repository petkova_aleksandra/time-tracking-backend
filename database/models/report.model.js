const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const reportSchema = new Schema(
  {
    year: { type: Number, required: true },
    month: {
      type: String,
      required: true,
      enum: ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER']
    },
    assignee: {
      userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
      username: { type: String, required: true },
      email: { type: String, required: true }
    },
    project: {
      projectId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Project' },
      shortName: { type: String, required: true }
    },
    weeks: [
      {
        weekNumber: { type: Number, required: true },
        startDate: { type: Date, required: true },
        endDate: { type: Date, required: true },
        weeklySpentTime: { type: Number, required: true }
      }
    ],
    montlySpentTime: { type: Number, required: true },
    dailySpentTime: [
      { type: Number, required: true }
    ],
    maxMontlySpentTime: { type: Number, required: true },
    maxWeeklySpentTime: { type: Number, required: true },
    maxDailySpentTime: { type: Number, required: true }
  },
  { timestamps: true }
);

module.exports = mongoose.model('Report', reportSchema);

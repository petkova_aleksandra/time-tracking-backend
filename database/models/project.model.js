const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const projectSchema = new Schema(
  {
    shortName: { type: String, required: true },
    fullName: { type: String, required: true },
    description: { type: String, required: true },
    status: { type: String, required: true, enum: ['NEW', 'IN_PROGRESS', 'CLOSED'], default: 'NEW' },
    users: [
      {
        userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
        username: { type: String, required: true },
        email: { type: String, required: true },
        maxDayHours: { type:Number, required: true },
        maxWeekHours: { type:Number, required: true },
        maxMonthHours: { type:Number, required: true }
      }
    ]
  },
  { timestamps: true }
);

module.exports = mongoose.model('Project', projectSchema);

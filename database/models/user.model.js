const mongoose = require('mongoose');

const userSchema = mongoose.Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    confirmed: { type: Boolean, default: false },
    password: { type: String, required: true },
    skype: { type: String },
    phone: { type: String },
    imagePath: { type: String },
    roles: [
      {
        projectId: { type: mongoose.Schema.Types.ObjectId, ref: 'Project' },
        name: { type: String, required: true, enum: ['ADMIN', 'PROJECT_ADMIN', 'USER'], default: 'USER' }
      }
    ],
    currentProjects: [
      { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Project' }
    ]
  },
  { timestamps: true }
);

module.exports = mongoose.model('User', userSchema);

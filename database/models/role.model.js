const mongoose = require('mongoose');

const roleSchema = mongoose.Schema(
  {
    user: {
      userId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
      username: { type: String, required: true },
      email: { type: String, required: true }
    },
    projectId: { type: mongoose.Schema.Types.ObjectId, ref: 'Project' },
    name: { type: String, required: true, enum: ['ADMIN', 'PROJECT_ADMIN', 'USER'], default: 'USER' } 
  },
  { timestamps: true }
);

module.exports = mongoose.model('Role', roleSchema);
